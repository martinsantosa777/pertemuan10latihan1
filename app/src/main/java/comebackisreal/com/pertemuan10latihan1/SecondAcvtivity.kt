package comebackisreal.com.pertemuan10latihan1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_second_acvtivity.*

class SecondAcvtivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second_acvtivity)

        val sharePrefHelper = SharePrefHelper(this, PrefFileName)
        btn_submit.setOnClickListener {
            if(edit_text_name.text.toString().isNullOrEmpty() || edit_text_email.text.toString().isNullOrEmpty() || edit_text_phone_number.text.toString().isNullOrEmpty()){
                Toast.makeText(this, "Data tidak boleh kosong", Toast.LENGTH_LONG).show()
            }
            else{
                dataUser.nama = edit_text_name.text.toString()
                dataUser.email = edit_text_email.text.toString()
                dataUser.noTelp = edit_text_phone_number.text.toString()
                sharePrefHelper.saveData()
                finish()
            }
        }

        btn_clear.setOnClickListener {
            edit_text_name.text.clear()
            edit_text_email.text.clear()
            edit_text_phone_number.text.clear()
        }
    }
}
