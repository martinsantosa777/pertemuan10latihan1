package comebackisreal.com.pertemuan10latihan1

import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.RadioButton
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

val PrefFileName = "MYFILEPREF01"
val dataUser : DataUser = DataUser()
class MainActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sharePrefHelper = SharePrefHelper(this, PrefFileName)

        sharePrefHelper.getData()
        updateUIData()
        changeCurrentBackground()

        btnEdit.setOnClickListener {
            var intent = Intent(this, SecondAcvtivity::class.java)
            startActivityForResult(intent, 1)
        }
        
        rdGroup.setOnCheckedChangeListener { group, checkedId ->
            val radio : RadioButton = findViewById(checkedId)
            dataUser.background = radio.text.toString()
            changeCurrentBackground()
            sharePrefHelper.saveBackground()
        }

        btnClear.setOnClickListener {
            sharePrefHelper.clearValues()
            updateUIData()
            changeCurrentBackground()
            rdBtnWhite.isChecked
        }
    }

    fun updateUIData(){
        txtNama.text = dataUser.nama
        txtEmail.text = dataUser.email
        txtNoTelp.text = dataUser.noTelp
    }

    fun changeCurrentBackground(){
        when(dataUser.background){
            "Red" -> MainBody.setBackgroundColor(Color.RED)
            "Green" -> MainBody.setBackgroundColor(Color.GREEN)
            "Blue" -> MainBody.setBackgroundColor(Color.BLUE)
            "Aqua" -> MainBody.setBackgroundColor(Color.rgb(69, 139, 116))
            "White" -> MainBody.setBackgroundColor(Color.WHITE)
        }
    }

    override fun onResume() {
        super.onResume()
        updateUIData()
    }
}