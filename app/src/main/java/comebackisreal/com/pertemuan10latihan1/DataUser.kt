package comebackisreal.com.pertemuan10latihan1

class DataUser{
    var nama : String
    var email : String
    var noTelp : String
    var background : String

    init {
        nama = "Tidak ada"
        email = "Tidak ada"
        noTelp = "Tidak ada"
        background = "White"
    }

    fun resetData(){
        nama = "Tidak ada"
        email = "Tidak ada"
        noTelp = "TIdak ada"
        background = "White"
    }
}