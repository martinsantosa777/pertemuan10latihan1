package comebackisreal.com.pertemuan10latihan1

import android.content.Context
import android.content.SharedPreferences
import android.util.Log

class SharePrefHelper(context: Context, name: String){
    val USER_NAME = "NAMA"
    val USER_EMAIL = "EMAIL"
    val USER_PHONE = "PHONE"
    val USER_BACKGROUND = "BACKGROUND"

    private val myPreferences : SharedPreferences
    init{
        myPreferences  = context.getSharedPreferences(name, Context.MODE_PRIVATE)
    }

    inline fun SharedPreferences.editMe(operation: (SharedPreferences.Editor) -> Unit) {
        var editMe = edit()
        operation(editMe)
        editMe.commit()
    }

    fun saveData(){
        myPreferences.editMe {
            it.putString(USER_NAME, dataUser.nama)
        }

        myPreferences.editMe {
            it.putString(USER_EMAIL, dataUser.email)
        }

        myPreferences.editMe {
            it.putString(USER_PHONE, dataUser.noTelp)
        }
    }

    fun saveBackground(){
        myPreferences.editMe {
            it.putString(USER_BACKGROUND, dataUser.background)
        }
    }

    fun getData(){
        try{
            dataUser.nama = myPreferences.getString(USER_NAME, "Tidak ada")
            dataUser.email = myPreferences.getString(USER_EMAIL, "TIdak ada")
            dataUser.noTelp = myPreferences.getString(USER_PHONE, "Tidak ada")
            dataUser.background = myPreferences.getString(USER_BACKGROUND, "White")
        }catch (e:Exception){
            Log.w("ERROR",e.toString())
        }
    }

    fun clearValues() {
        dataUser.resetData()
        myPreferences.edit().clear().commit()
    }

}
